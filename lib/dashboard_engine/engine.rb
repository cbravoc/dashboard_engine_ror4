module DashboardEngine
  class Engine < ::Rails::Engine
    isolate_namespace DashboardEngine

    initializer "asset precompile", :group => :all do |app|
			app.config.assets.paths << Rails.root.join('app', 'assets', 'fonts')

		  app.config.assets.precompile += %w(
		    *.svg
		    *.eot
		    *.woff
		    *.ttf
		    *.gif
		    *.png
		    *.ico
		    dashboard_engine/compiled/index.css
		    dashboard_engine/lib/fullcalendar.print.css
		    dashboard_engine/lib/fullcalendar.print.css
		    dashboard_engine/compiled/calendar.css
		    dashboard_engine/highcharts.js
		    dashboard_engine/highcharts_options.js
		  )
		end
  end
end
