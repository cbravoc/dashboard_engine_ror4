Highcharts.setOptions({
  lang: {
    weekdays: ["Domingo", "Lunes", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    shortMonths: [ "Ene" , "Feb" , "Mar" , "Abr" , "May" , "Jun" , "Jul" , "Ago" , "Sep" , "Oct" , "Nov" , "Dec"],
    loading: "Cargando..."
  }
});