// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require dashboard_engine/bootstrap.min
//= require dashboard_engine/jquery.knob
//= require dashboard_engine/jquery.flot
//= require dashboard_engine/jquery.flot.stack
//= require dashboard_engine/jquery.flot.resize
//= require dashboard_engine/jquery-ui-1.10.2.custom.min
//= require dashboard_engine/morris.min
//= require dashboard_engine/raphael-min
//= require dashboard_engine/fullcalendar.min
//= require dashboard_engine/jquery.dataTables.min
//= require dashboard_engine/theme
//= require dashboard_engine/bootstrap.datepicker

$(function () {
	$.fn.datepicker.dates['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy",
		clear: "Borrar",
		weekStart: 1,
		format: "dd/mm/yyyy"
	};

	$('.input-datepicker').datepicker({
    language: 'es'
	});

	$('.input-datepicker').datepicker().on('changeDate', function (ev) {
	  $(this).datepicker('hide');
	});
});