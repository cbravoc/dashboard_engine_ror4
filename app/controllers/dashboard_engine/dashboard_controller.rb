require_dependency "dashboard_engine/application_controller"

module DashboardEngine
  class DashboardController < ApplicationController
    def index
    end

    def charts
		end

		def users
		end

		def forms
		end

		def gallery
		end

		def calendar
		end

		def tables
		end

		def ui_elements
		end

		def my_info
		end

		def extras
		end

		def new_user
		end

		def iframe
	    respond_to do |format|
	      format.html { render "dashboard/iframe", :layout => false }
	    end
	  end
  end
end
